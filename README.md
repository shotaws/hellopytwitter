変数searchに入っている文字列でTwitter検索APIを叩くプログラム

-------------------------------------

* 「python tweet_crawler.py」で起動


* 環境：

    * python 2.7.12

    * requests-oauthlibという Python 用の OAuth 認証ライブラリ

* 追加で必要な処理：

    1. Twitterアカウントを新規に取得

    1. アカウントに携帯電話番号を設定（登録していないと手順3で4つのキーが取得できない）

    1. https://apps.twitter.com/ でOAuth認証に必要な4つのキーを取得
（Keys and Access Tokensで、Application SettingsのConsumer Key (API Key)、Application SettingsのConsumer Secret (API Secret)、Your Access TokenのAccess Token、Your Access TokenのAccess Token Secret）

    1. その4つのキーをconfig.jsonに明記

* 主な処理の流れ

    1. 起動するとmain()関数が呼ばれる

    1. tweet_search()関数が呼ばれる

    1. https://api.twitter.com/1.1/search/tweets.json?q=unicode(search_word)&lang=ja&result_type=recent&count=15 の部品化

    1. create_oath_session()関数を呼び、config.jsonから4つのキーを読み取ってOAuth1Sessionモジュールのoath変数を作成する

    1. OAuth1Sessionモジュールには指定したURLにGETリクエストを飛ばすget()メソッドがあるので、手順3で作った部品を使って、URLを組み立てつつ、GETリクエストを飛ばす。そしてresponse変数にレスポンスを格納する

    1. HTTPステータスコードが200（OK）でなければエラーを表示する

    1. HTTPステータスコードが200であった場合、responceの中のtext変数（JSON形式）の中身をtweets変数に読み込む

    1. main関数に戻り、必要なデータを取り出して表示する